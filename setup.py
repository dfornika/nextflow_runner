from setuptools import setup

setup(
    namae='nextflow_runner',
    packages=['nextflow_runner'],
    include_package_data=True,
    install_requires = [
        'flask'
    ]
)