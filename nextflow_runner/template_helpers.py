import dateutil.parser
import datetime

def prettyprint_isodate(isodate_string):
    date = dateutil.parser.parse(isodate_string)
    return datetime.datetime.strftime(date, "%Y-%m-%d %H:%M")