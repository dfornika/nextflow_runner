import boto3
from boto3.dynamodb.conditions import Key
import json
import decimal
import datetime
PROFILE_NAME = 'test'
DYNAMODB_TABLES = 'nextflow_workflow_submissions'

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if abs(o) % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

session = boto3.Session(profile_name=PROFILE_NAME)
dynamodb = session.resource('dynamodb')
table = dynamodb.Table(DYNAMODB_TABLES)

def get_user_workflows(email):
    response = table.query(
        IndexName='email',
        KeyConditionExpression=Key('email').eq(email)
    )
    return response['Items']


def add_workflow_submission_to_dynamodb(workflow, data_path, user_email):
    status = 'submitted'
    created_at = datetime.datetime.utcnow().isoformat()
    updated_at = datetime.datetime.utcnow().isoformat()

    if workflow in ['snp_phylogeny', 'assembly']:
        full_data_path = '{0}/fastqs'.format(data_path)
    elif workflow in ['abricate']:
        full_data_path = '{0}/assemblies'.format(data_path)

    response = table.put_item(
    Item={
            'workflow': workflow,
            'data_path': full_data_path,
            'email': user_email,
            'created_at': created_at,
            'updated_at': updated_at,
            'status': status
        }
)
