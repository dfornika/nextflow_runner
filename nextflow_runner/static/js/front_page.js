$(document).ready(function() {

    // inititialize modals
    $('.modal').modal();

    // populate workflows table
    function get_previous_workflows(){
        $.ajax({
            url: "/get_previous_workflows",
        }).done(function(response) {
            $('#workflows_table').html(response);
            // initialize datatable
            $('#workflow_table').DataTable(
                {
                    order: [[ 5, 'desc']]
                }
            );
        });
    }
    get_previous_workflows();

    $("#refresh_workflows_table").click(function(e) {
        get_previous_workflows();
    });

    // submit job form
    function submit_job(){
        $.ajax({
            method: "POST",
            url: "/",
            data: {
                workflow: $('select[name="workflow"]').val(),
                raw_data_path: $('select[name="raw_data_path"]').val(),
                fasta_data_path: $('select[name="fasta_data_path"]').val(),
                reference_path: $('select[name="reference_path"]').val(),
                abricate_database: $('select[name="abricate_database"]').val()
            }
        }).done(function(response) {
            if (response.status == 'alert'){
                $("#notifications .modal-content").html("<h4 class='red darken-3 white-text center'>Alert</h4>" + response.message);
                $("#notifications").modal('open');
            } else if (response.status == 'notice'){
                $("#notifications .modal-content").html("<h4 class='purple darken-3 white-text center'>Notice</h4>" + response.message);
                $("#notifications").modal('open');
            }
            get_previous_workflows().delay(1000);
        }).fail (function(){
            alert('An error occured')
        });
        return false;
    }

    $("#job_submit_button").click(function(e) {
        submit_job();
    });

    //  hide and show relevant dropdowns based on workflow

    function display_data_paths(){
        if ($("#workflow").val() == 'assembly') {
            $("#raw_data_div").show();
            $("#fasta_data_div").hide();
            $("#reference_path_div").hide();
            $("#abricate_database_div").hide();
        } else if ($("#workflow").val() == 'snp_phylogeny') {
            $("#raw_data_div").show();
            $("#fasta_data_div").hide();
            $("#reference_path_div").show();
            $("#abricate_database_div").hide();
        } else if ($("#workflow").val() == 'abricate') {
            $("#raw_data_div").hide();
            $("#fasta_data_div").show();
            $("#reference_path_div").hide();
            $("#abricate_database_div").show();
        }
    }

    $("#workflow").change(function() {
        display_data_paths()
    });

    // call datapaths show/hide function on startup
    display_data_paths()

    // get eligble fastqs s3 paths

    function get_s3_raw_data_paths(){
        $.ajax('/_get_raw_data_paths')
        .done(function(data) {
            $("#raw_data_path").replaceWith(data);
            $('select').formSelect();
        })
        .fail(function() {
            alert( "error" );
        });
        return false;
    }
    // get eligble fasta s3 paths
    function get_s3_fasta_data_paths(){
        $.ajax('/_get_fasta_data_paths')
        .done(function(data) {
            $("#fasta_data_path").replaceWith(data);
            $('select').formSelect();
        })
        .fail(function() {
            alert( "error" );
        });
        return false;
    }

    // get reference sequences
    function get_s3_reference_paths(){
        $.ajax('/_get_reference_paths')
        .done(function(data) {
            $("#reference_path").replaceWith(data);
            $('select').formSelect();
        })
        .fail(function() {
            alert( "error" );
        });
        return false;
    }

    // call functions to populate dropdowns
    get_s3_raw_data_paths();
    get_s3_fasta_data_paths();
    get_s3_reference_paths();

} );